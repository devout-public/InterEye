# Installation

## System Requirements
 - Windows10 (OS build 19042.1348)
 - Docker Desktop (4.2.0) (https://docs.docker.com/desktop/windows/install/)
 - WSL 2 backend (https://docs.microsoft.com/en-us/windows/wsl/install-manual#step-4---download-the-linux-kernel-update-package)
 - Git client


## Application
InterEye application is composed of three different server types:
- Integrator
- PAM
- Provider

There is a single docker image, containing a executable jar file, that serves the three server types/roles
based on proper configuration. According to the selected role, InterEye displays only the
relevant features on the user interface of the particular role and is restricted to perform only the appropriate
operations.

The installation is based on appropriate docker-compose files. Each docker-compose contains two services:
1) the MySQL server,
2) the InterEye application


## Integrator
The parameters that needs to be updated are:
- APP_IP: the ip of server,
- SERVER_PORT and ports: the port that will be listening on.


```sh
version: '3.7'

services:
  mysql-integrator:
    container_name: mysql-integrator
    image: mysql:latest
    environment:
      MYSQL_ROOT_PASSWORD: admin
      MYSQL_DATABASE: integrator
      MYSQL_USER: intereye
      MYSQL_PASSWORD: intereye
    restart: on-failure
    ports:
      - "3308:3306"
    volumes:
      - './db/data:/var/lib/mysql'
      - './db/my.cnf:/etc/mysql/conf.d/my.cnf'
      - './db/sql:/docker-entrypoint-initdb.d'

  integrator:
    container_name: integrator
    image: registry.gitlab.com/devout-public/intereye/lshdb-app:v1
    environment:
      DB_SCHEMA: integrator
      DB_URL: "mysql-integrator:3306"
      APP_MODE: Integrator
      APP_ID: 1
      APP_NAME:
      APP_IP: localhost
      SERVER_PORT: 9090
    restart: on-failure
    ports:
      - "9090:9090"
    depends_on:
      - mysql-integrator
    volumes:
      - './stores:/opt/service/data/stores'
```

## PAM
The parameters that needs to be updated are:
- APP_IP: the ip of server,
- SERVER_PORT and ports: the port that will be listening on.

```sh
version: '3.7'

services:
  mysql-pam:
    container_name: mysql-pam
    image: mysql:latest
    environment:
      MYSQL_ROOT_PASSWORD: admin
      MYSQL_DATABASE: pam
      MYSQL_USER: intereye
      MYSQL_PASSWORD: intereye
    restart: on-failure
    ports:
      - "3309:3306"
    volumes:
      - './db/data:/var/lib/mysql'
      - './db/my.cnf:/etc/mysql/conf.d/my.cnf'
      - './db/sql:/docker-entrypoint-initdb.d'

  pam:
    container_name: pam
    image: registry.gitlab.com/devout-public/intereye/lshdb-app:v1
    environment:
      DB_SCHEMA: pam
      DB_URL: "mysql-pam:3306"
      APP_MODE: PAM
      APP_ID: 2
      APP_NAME:
      APP_IP: localhost
      SERVER_PORT: 9091
    restart: on-failure
    ports:
      - "9091:9091"
    depends_on:
      - mysql-pam
    volumes:
      - './stores:/opt/service/data/stores'
```

## Providers
The parameters that needs to be updated are:
- APP_IP: the ip of server,
- SERVER_PORT and ports: the port that will be listening on,
- APP_ID: A unique Id for each provider, starting from 100 and increment by 100. For example, Provider-A has APP_ID=100, Provider-B has APP_ID=200, Provider-C has APP_ID=300, etc.
- APP_NAME:The application/provider name,
- INTEGRATOR_URL: the url of Integrator,
- PAM_URL: the url of PAM
- replace **provider-a** with the name of provider, e.g vodafone

 ```sh
 version: '3.7'

services:
  mysql-provider-a:
    container_name: mysql-provider-a
    image: mysql:latest
    environment:
      MYSQL_ROOT_PASSWORD: admin
      MYSQL_DATABASE: provider
      MYSQL_USER: intereye
      MYSQL_PASSWORD: intereye
    restart: on-failure
    ports:
      - "3310:3306"
    volumes:
      - './db/data:/var/lib/mysql'
      - './db/my.cnf:/etc/mysql/conf.d/my.cnf'
      - './db/sql:/docker-entrypoint-initdb.d'

  provider-a:
    container_name: provider-a
    image: registry.gitlab.com/devout-public/intereye/lshdb-app:v1
    environment:
      DB_SCHEMA: provider
      DB_URL: 'mysql-provider-a:3306'
      APP_MODE: Provider
      APP_ID: 100
      APP_NAME: provider-a
      APP_IP: 172.24.224.1
      SERVER_PORT: 9092
      INTEGRATOR_URL: 'http://172.24.224.1:9090'
      PAM_URL: 'http://172.24.224.1:9091'
    restart: on-failure
    ports:
      - "9092:9092"
    depends_on:
      - mysql-provider-a
    volumes:
      - './stores:/opt/service/data/stores'
      - './log:/opt/service/log'
 ```

### Example

```sh
 version: '3.7'

services:
  mysql-vodafone:
    container_name: mysql-vodafone
    image: mysql:latest
    environment:
      MYSQL_ROOT_PASSWORD: admin
      MYSQL_DATABASE: provider
      MYSQL_USER: intereye
      MYSQL_PASSWORD: intereye
    restart: on-failure
    ports:
      - "3310:3306"
    volumes:
      - './db/data:/var/lib/mysql'
      - './db/my.cnf:/etc/mysql/conf.d/my.cnf'
      - './db/sql:/docker-entrypoint-initdb.d'

  vodafone:
    container_name: vodafone
    image: registry.gitlab.com/devout-public/intereye/lshdb-app:v1
    environment:
      DB_SCHEMA: provider
      DB_URL: 'mysql-vodafone:3306'
      APP_MODE: Provider
      APP_ID: 100
      APP_NAME: vodafone
      APP_IP: 172.24.224.1
      SERVER_PORT: 9092
      INTEGRATOR_URL: 'http://172.24.224.1:9090'
      PAM_URL: 'http://172.24.224.1:9091'
    restart: on-failure
    ports:
      - "9092:9092"
    depends_on:
      - mysql-vodafone
    volumes:
      - './stores:/opt/service/data/stores'
      - './log:/opt/service/log'
 ```
